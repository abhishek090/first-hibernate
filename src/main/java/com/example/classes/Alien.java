package com.example.classes;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class Alien {

    @Id
    private int id;

    @Column(name = "Alien_name")
    private AlienName name;

    private int age;

}
