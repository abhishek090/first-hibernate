package com.example.classes;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args){
        System.out.println("Hii there !");

        AlienName name = new AlienName();
        name.setFName("Abhishek");
        name.setMName("pawankumar");
        name.setLName("sheelwant");

        Alien alien = new Alien();

        alien.setId(1);
        alien.setAge(400);
        alien.setName(name);

        Configuration configuration = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Alien.class);

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(alien);

        transaction.commit();
    }
}
