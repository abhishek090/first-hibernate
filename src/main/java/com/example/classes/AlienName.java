package com.example.classes;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

@Getter
@Setter
@Embeddable
public class AlienName {
    private String fName;

    private String mName;

    private String lName;

    @Override
    public String toString() {
        return "AlienName{" +
                "fName='" + fName + '\'' +
                ", mName='" + mName + '\'' +
                ", lName='" + lName + '\'' +
                '}';
    }

}
